var seatsPricing =
{
    sofa: 0,
    armchair: 0
}

var totalCost = 0;

$(function(){
  $.getJSON("a42580.json" ,function(d){

    var t = $("<table/>", { id: "seatsTable" }).css("margin-left", "5%").appendTo("#theatre");

    for(var i=0; i < d.rowLabels.length; i++)
    {
        var tr = $("<tr/>")
            .append($("<th/>",{ text:d.rowLabels[i] }))
            .appendTo(t);

        var tmap = d.tmap[i];
        var umap = d.umap[i];
        var seatNum = 1;

        for(var empty = tmap.length - 1; empty > 0; --empty)
        {
            var pre = tmap;
            if(tmap[empty] === ' ')
            {
                tmap = tmap.substring(0, tmap.length - 1);
            }
            else
            {
                break;
            }
        }

        for(var j = 0; j < tmap.length; j++)
        {
            var td = $("<td/>");

            if(tmap[j] === "L" || tmap[j] === "R")
            {
                if(umap[j] === "O" || umap[j] === "M")
                {
                    $(td).addClass("sofa-vacant");
                }
                else if(umap[j] === "X")
                {
                    $(td).addClass("sofa-taken");
                }
                
                j += 1;
                td.text(seatNum + "  " + (seatNum + 1).toString());
                seatNum += 2;
            }
            else if(tmap[j] === "A")
            {
                if(umap[j] === "O" || umap[j] === "M")
                {
                    $(td).addClass("armchair-vacant");
                }
                else if(umap[j] === "X")
                {
                    $(td).addClass("armchair-taken");
                }

                td.text(seatNum);
                seatNum++;
            }
            else
            {
                $(td).addClass("empty");
                td.text(" ");

                if(tmap[j + 2] === ' ')
                {
                    j += 2;
                }
                else if(tmap[j + 1] === ' ')
                {
                    j += 1;
                }
            }
            tr.append(td);
        }
    }

    var buttonsDiv = $("<div/>" , { class: "row" });
    $("<button/>", {text: "Continue", class: "btn btn-success disabled col-md-1" , id: "continue"})
        .appendTo(buttonsDiv); 

    $("<button/>", {text: "Back", class: "btn btn-danger col-md-1" , id: "back"})
        .appendTo(buttonsDiv);
        
    $(buttonsDiv).appendTo("#wrapper");
    $("#continue").css( { float: "right", margin: "30 40 10 10" });
    $("#back").css({ float: "left", margin: "30 10 10 37" });

    $("#poster").attr("src", d.image);
    $("#title").text(d.title);

    var date = new Date(d.date);
    $("#dateTime").text(date.toLocaleDateString() + " " + formatTime(date));

    $("#screen").text(d.screen);
    $("#runTime").text(d.runtime + " min");
    $("#rating").text(d.rating);

    seatsPricing.sofa = d.pricing.L;
    seatsPricing.armchair = d.pricing.A;

    $("#sofasSelected").text("0");
    $("#armchairsSelected").text("0");

    $("#total").text("Total: "  + "$0.00");

    $("<div/>").css({ "height": "30px",
        "background-color": "white",
        "margin-left": "20%",
        "margin-right": "20%",
        "margin-top": "10px",
        "text-align": "center",
        "font-weight": "bold" }).text("Screen").appendTo("#theatre");

    canBeClicked();

    $("td").on("mousedown", function()
    {
        var className = $(this).attr("class");

        if(className == "sofa-taken" || className == "armchair-taken" 
            || className == "empty")
        {
            return;
        }
        else
        {
            $(this).removeClass();

            switch(className)
            {
                case "sofa-vacant":
                    $(this).addClass("sofa-vacant-clicked");
                    break;
                case "sofa-chosen":
                    $(this).addClass("sofa-chosen-clicked");
                    break;
                case "armchair-vacant":
                    $(this).addClass("armchair-vacant-clicked");
                    break;
                case "armchair-chosen":
                    $(this).addClass("armchair-chosen-clicked");
                    break;
                default:
                    break;
            }
        }
    }).on("mouseup", function()
    {
        var className = $(this).attr("class");

        if(className == "sofa-taken" || className == "armchair-taken" 
            || className == "empty")
        {
            return;
        }
        else
        {
            $(this).removeClass();

            switch(className)
            {
                case "sofa-vacant-clicked":
                    $(this).addClass("sofa-chosen");
                    totalCost += seatsPricing.sofa;
                    break;
                case "sofa-chosen-clicked":
                    $(this).addClass("sofa-vacant");
                    totalCost -= seatsPricing.sofa;
                    break;
                case "armchair-vacant-clicked":
                    $(this).addClass("armchair-chosen");
                    totalCost += seatsPricing.armchair;
                    break;
                case "armchair-chosen-clicked":
                    $(this).addClass("armchair-vacant");
                    totalCost -= seatsPricing.armchair;
                    break;
                default:
                    break;
            }

            $("#total").text("Total: "  + "$" + totalCost.toFixed(2).toString());
            $("#sofasSelected").text($(".sofa-chosen").length);
            $("#armchairsSelected").text($(".armchair-chosen").length);

            canBeClicked();
        }
    });

    $("#continue").click(function()
    {
        if($(this).hasClass("disabled"))
        {
            return;
        }
        else
        {
            $(".sofa-chosen, .armchair-chosen").each(function(i ,el)
            {
                var parent = $(el).parent();
                var th = $(parent).children().first();

                var seatsString = $(el).text();

                var displayString = "Row: " + $(th).text() + " Seats: " + seatsString.replace(" ", " and ");

                $("<li/>").text(displayString).appendTo("#seatsConfirm");
            });

            $("#confirmModal").modal("show");
        }
    });

    $("#back").click(function()
    {
        alert("Going back...");
    });

    $("#confirmModal").on("hidden.bs.modal", function() 
    {
        $("#seatsConfirm").empty();
    });

    $("#modalConf").click(function()
    {
        alert("Proceeding to payment...");
        $("#confirmModal").modal("hide");
    });
})});

function canBeClicked()
{
    if ($(".sofa-chosen").length > 0 ||
        $(".armchair-chosen").length > 0) 
    {
        $("#continue").removeClass("disabled");
        return true;
    }
    else 
    {
        $("#continue").addClass("disabled");
        return false;
    }
}

function formatTime(dateObject)
{
    if(typeof dateObject != "object")
    {
        return undefined;
    }
    else
    {
        var time = dateObject.toLocaleTimeString();

        var hoursPart = time.substring(0, 4);
        var secondsPart = time.substring(6, time.length);

        return hoursPart + secondsPart.substring(1, secondsPart.length);
    }
}